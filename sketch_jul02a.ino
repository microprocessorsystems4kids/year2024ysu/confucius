#include <Arduino.h>
#include <U8g2lib.h>

#ifdef U8X8_HAVE_HW_I2C
#include <Wire.h>
#endif

U8G2_SSD1306_128X64_NONAME_F_HW_I2C u8g2(U8G2_R0, /* reset=*/ U8X8_PIN_NONE);

/* constants */
constexpr unsigned long DELAY_TIME = 1000; // the time the delay started
constexpr int width = 18;
constexpr int height = 4;
constexpr int maxLength = 10; // maximum length of the snake
// Define button pins
constexpr int btnLeftPin = 3;
constexpr int btnUpPin = 4;
constexpr int btnRightPin = 5;
constexpr int btnDownPin = 6;
constexpr int btnTitlePin = 7; // New button pin for displaying the title

/* enum */
enum eDirection { STOP = 0, LEFT, RIGHT, UP, DOWN };

/*================================================================*/
/* Game                                                           */
/*================================================================*/

class Game {
public:
    Game();
    virtual ~Game(void) = default;
    Game(const Game&) = delete;
    Game(Game&&) = delete;
    /* method */
    void SetupGame(void);
    void DrawGame(void);
    void InputGame(void);
    void LogicGame(void);
    void DisplayTitle(void); // New method to display the title
    unsigned long delayStart; // the time the delay started
    bool gameOver;
    int score;
private:
    /* variables */
    eDirection dir;
    int snakeX[maxLength], snakeY[maxLength]; // arrays to store the snake's position
    int snakeLength;
    int x, y, fruitX, fruitY;
};

/*================================================================*/
/* Game Constructor                                               */
/*================================================================*/

Game::Game() : delayStart(millis())
{/* Should be Empty */}
  
void Game::SetupGame() {
    gameOver = false;
    dir = STOP;
    x = width / 2;
    y = height / 2;
    fruitX = random(width); // Use random() function instead of rand()
    fruitY = random(height); // Use random() function instead of rand()
    score = 0;
    snakeLength = 1; // initial length of the snake
    snakeX[0] = x;
    snakeY[0] = y;
}

void Game::DrawGame() {
    u8g2.clearBuffer(); // clear the internal memory
    u8g2.setFont(u8g2_font_ncenB08_tr); // choose a suitable font

    // Draw the boundaries
    for (int i = 0; i < width + 2; i++) {
      u8g2.drawGlyph(i * 6, 10, '#');
    }

    for (int i = 0; i < height; i++) {
        u8g2.drawGlyph(0, (i + 1) * 10, '#');
        for (int j = 0; j < width; j++) {
            if (i == y && j == x) {
                u8g2.drawGlyph((j + 1) * 6, (i + 1) * 10, 'O');//snake
            }
            else if (i == fruitY && j == fruitX) {
                u8g2.drawGlyph((j + 1) * 6, (i + 1) * 10, 'F');//food
            }
            else {
                bool print = false;
                for (int k = 0; k < snakeLength; k++) {
                    if (snakeX[k] == j && snakeY[k] == i) {
                        u8g2.drawGlyph((j + 1) * 6, (i + 1) * 10, 'o');
                        print = true;
                    }
                    else { /* do nothing */ }
                }
                if (!print) {
                  u8g2.drawGlyph((j + 1) * 6, (i + 1) * 10, ' ');
                }
                else { /* do nothing */ }
            }
        }
        u8g2.drawGlyph((width + 1) * 6, (i + 1) * 10, '#');
    }
    for (int i = 0; i < width + 2; i++) {
      u8g2.drawGlyph(i * 6, (height + 1) * 10, '#');
    }

    // Print the score
    u8g2.setCursor(0, (height + 2) * 10);
    u8g2.print("Score: ");
    u8g2.print(score);
    u8g2.sendBuffer(); // transfer internal memory to the display
}

void Game::InputGame() {
    // Check buttons for direction control
    if (digitalRead(btnLeftPin) == HIGH) {
        dir = LEFT;
    } 
    else if (digitalRead(btnRightPin) == HIGH) {
        dir = RIGHT;
    } 
    else if (digitalRead(btnUpPin) == HIGH) {
        dir = UP;
    } 
    else if (digitalRead(btnDownPin) == HIGH) {
        dir = DOWN;
    }
    else { /* do nothing */ }
}

void Game::LogicGame() {
    int prevX = snakeX[0];//更新蛇移动的位置 ，保存蛇当前位置 
    int prevY = snakeY[0];//保存蛇头当前位置 
    int prev2X, prev2Y;//更新蛇头位置的临时值 
    snakeX[0] = x;
    snakeY[0] = y;
    for (int i = 1; i < snakeLength; i++) {
        prev2X = snakeX[i];
        prev2Y = snakeY[i];
        snakeX[i] = prevX;
        snakeY[i] = prevY;
        prevX = prev2X;
        prevY = prev2Y;
    }
    switch (dir) {//移动蛇 
    case LEFT:
        x--;
        break;
    case RIGHT:
        x++;
        break;
    case UP:
        y--;
        break;
    case DOWN:
        y++;
        break;
    default:
        break;
    }

    if (x >= width) {
      x = 0; 
    }
    else if (x < 0) {
      x = width - 1;//触碰边界
    }
    else { /* do nothing - cannot be less than 0 and more than width */ } 

    if (y >= height) {
      y = 0; 
    }
    else if (y < 0) {
      y = height - 1;//触碰边界 
    }
    else { /* do nothing */ }

    for (int i = 1; i < snakeLength; i++) {//碰到自身
        if (snakeX[i] == x && snakeY[i] == y) {
            gameOver = true;
        }
    }

    if (x == fruitX && y == fruitY) {//得分，改变F位置 
        score += 10;
        fruitX = random(width-1); // Use random() function instead of rand()
        fruitY = random(height-1); // Use random() function instead of rand()
        snakeLength++;
    }
    else { /* do nothing */ }
}

void Game::DisplayTitle() {
    u8g2.clearBuffer();
    u8g2.setFont(u8g2_font_ncenB08_tr);
    u8g2.setCursor(0, 10);
    u8g2.print("Stop");
    u8g2.sendBuffer();
}

void setup() {
    u8g2.begin();
    Serial.begin(9600);

    // Initialize button pins
    pinMode(btnLeftPin, INPUT);
    pinMode(btnRightPin, INPUT);
    pinMode(btnUpPin, INPUT);
    pinMode(btnDownPin, INPUT);
    pinMode(btnTitlePin, INPUT); // Initialize the new button pin

    randomSeed(analogRead(0)); // Seed the random number generator
}

void loop() {
  
    Game game;
    

    game.SetupGame();

    while (!game.gameOver) {
        game.InputGame();
        
        if ((millis() - game.delayStart) >= DELAY_TIME) {
            game.LogicGame();
            game.DrawGame();
            game.delayStart = millis();
        }
        else { /* do nothing */ }

        if (digitalRead(btnTitlePin) == HIGH) {
            game.DisplayTitle();
            delay(500); // Debounce delay
        }
        else { /* do nothing */ }
    } 

    u8g2.clearBuffer();
    u8g2.setFont(u8g2_font_ncenB08_tr);
    u8g2.setCursor(0, 30);
    u8g2.print("Game Over!");
    u8g2.setCursor(0, 50);
    u8g2.print("Score: ");
    u8g2.print(game.score);
    u8g2.sendBuffer();
}
